var team=[]
var dataAr=[]
function showData()
{
  
var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
var theUrl = 'https://linde.virtuallive.in:3002/gamefind';
xmlhttp.open("POST", theUrl);
xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
const queryParamsString = window.location.search.substr(1);
// alert(queryParamsString)
xmlhttp.onreadystatechange = function() { // Call a function when the state changes.
    if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
       console.log("sasasas",JSON.parse(this.responseText))
        dataAr=JSON.parse(this.responseText)
       console.log("sss",dataAr.data.docs[0])
       team=dataAr.data.finalData
      
      if(team.length>0) 
      {
       
        f()
      }
    }
}

xmlhttp.send(JSON.stringify({ "gameName": `${queryParamsString}`,"email":`${localStorage.getItem('email')}`}));

}
showData();



    function f()
    {
   
const randomEmoji = () => {
  const emojis = ['👏', '👍', '🙌', '🤩', '🔥', '⭐️', '🏆', '💯'];
  let randomNumber = Math.floor(Math.random() * emojis.length);
  return emojis[randomNumber];
};

team.forEach(member => {
  let newRow = document.createElement('li');
  newRow.classList = 'c-list__item';
  newRow.innerHTML = `
		<div class="c-list__grid">
			<div class="c-flag c-place u-bg--transparent">${member.rank}</div>
			<div class="c-media">
				 <img class="c-avatar c-media__img" src="https://toppng.com/uploads/preview/roceed-mobile-user-icon-11562925984aa78xqe1rs.png" />
				<div class="c-media__content">
					<div class="c-media__title">${member.name}</div>
					<a class="c-media__link u-text--small"  target="_blank">score ${member.userScore}</a>
				</div>
			</div>
			<div class="u-text--right c-kudos">
				<div class="u-mt--8">
					<strong>${member.email}</strong> ${randomEmoji()}
				</div>
			</div>
		</div>
	`;
  if (member.rank === 1) {
    newRow.querySelector('.c-place').classList.add('u-text--dark');
    newRow.querySelector('.c-place').classList.add('u-bg--yellow');
    newRow.querySelector('.c-kudos').classList.add('u-text--yellow');
  } else if (member.rank === 2) {
    newRow.querySelector('.c-place').classList.add('u-text--dark');
    newRow.querySelector('.c-place').classList.add('u-bg--teal');
    newRow.querySelector('.c-kudos').classList.add('u-text--teal');
  } 
  list.appendChild(newRow);
});

// Find Winner from sent kudos by sorting the drivers in the team array
let sortedTeam = team.sort((a, b) => b.sent - a.sent);
let winner = sortedTeam[0];


const winnerCard1 = document.getElementById('winner1');
winnerCard1.innerHTML = `
<div class="u-text--left">
<div class="u-text--small">
name:<br> ${dataAr.data.docs[0].name} <br></div>

</div>
<div class="u-text--right">
<div class="u-text--small">My Score</div>
<h2>${dataAr.data.docs[0].userScore}</h2>
</div>
`;
const winnerCard = document.getElementById('winner');
winnerCard.innerHTML = `
	<div class="u-text-small u-text--medium u-mb--16">Top Sender Last Week</div>
	<img class="c-avatar c-avatar--lg" src="https://toppng.com/uploads/preview/roceed-mobile-user-icon-11562925984aa78xqe1rs.png" />
	<h3 class="u-mt--16">${winner.name}</h3>
	<span class="u-text--teal u-text--small">score: ${winner.userScore}</span>
`;
    }
  //   <div class="c-card__body">
  //   <div class="u-display--flex u-justify--space-between">
  //     <div class="u-text--left">
  //       <div class="u-text--small">My Rank</div>
  //       <h2>3rd Place</h2>
  //     </div>
  //     <div class="u-text--right">
  //       <div class="u-text--small">My Score</div>
  //       <h2>24</h2>
  //     </div>
  //   </div>
  // </div>